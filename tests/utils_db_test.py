from sqlalchemy import create_engine
import psycopg2
import os
from sqlalchemy.orm import sessionmaker

engine = create_engine('postgresql://ani:u14k98uc18@localhost:5432/ani_test')


Psql_DB_USER = os.getenv('db_user')
Psql_DB_PASSWORD = os.getenv('db_password')
Psql_DB_NAME = "ani_test"
Psql_DB_HOST = os.getenv('db_host')

config = {
    'host': Psql_DB_HOST,
    'user': Psql_DB_USER,
    'password': Psql_DB_PASSWORD,
    'database': Psql_DB_NAME
}


def db_session():
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


def db_read(query):
    return engine.execute(query)


def db_write(query, params=None):
    cnx = None
    cursor = None
    try:
        cnx = psycopg2.connect(**config)
        cursor = cnx.cursor()
        try:
            cursor.execute(query, params)
            cnx.commit()
            cursor.close()
            cnx.close()
            return True

        except psycopg2.Error as err:
            print(err)
            cursor.close()
            cnx.close()
            return False

    except psycopg2.Error as err:
        print(err)
        return False
    finally:
        if cursor:
            cursor.close()
        if cnx:
            cnx.close()
            print("Connection closed")