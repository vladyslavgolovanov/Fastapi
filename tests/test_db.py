from unittest import TestCase
import psycopg2
from mock import patch
from tests import utils_db_test
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

Psql_DB_USER = "ani"
Psql_DB_PASSWORD = "u14k98uc18"
Psql_DB_NAME = "ani_test"
Psql_DB_HOST = "localhost"


class MockDB(TestCase):
    @classmethod
    def setUpClass(cls):
        cnx = psycopg2.connect(
            database="ani",
            host=Psql_DB_HOST,
            user=Psql_DB_USER,
            password=Psql_DB_PASSWORD,

        )
        cnx.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cursor = cnx.cursor()

        try:
            cursor.execute("DROP DATABASE {}".format(Psql_DB_NAME))
            cursor.close()
            print("DB dropped")
        except psycopg2.Error as err:
            print("{}{}".format(Psql_DB_NAME, err))
        cursor = cnx.cursor()
        try:
            cursor.execute(
                """CREATE DATABASE {}""".format(Psql_DB_NAME))
            cursor.close()
            cnx.close()
        except psycopg2.Error as err:
            print("Failed creating database: {}".format(err))
        connection_test = psycopg2.connect(
            database=Psql_DB_NAME,
            host=Psql_DB_HOST,
            user=Psql_DB_USER,
            password=Psql_DB_PASSWORD,

        )
        cursor_test = connection_test.cursor()

        query = """

        CREATE SCHEMA ani;

        -- table bestellung

        CREATE TABLE ani.bestellung (
    person_id bigint NOT NULL,
    bestell_nr numeric NOT NULL,
    shop_id integer,
    transaktion_nr character varying(100),
    bestelldatum timestamp without time zone,
    rechnungsbetrag_netto numeric(9,2),
    versandkosten_brutto numeric(9,2),
    waehrung character varying(30),
    abweichende_lieferadresse bigint,
    abweichende_rechnungsadresse bigint,
    auftragsstatus_id integer,
    cleared integer,
    bezahlart_id integer,
    versand_id integer,
    rechnungsbetrag_brutto numeric(9,2),
    versandkosten_netto numeric(9,2),
    bemerkung text,
    kundenbemerkung text,
    interne_bemerkung text,
    temporaere_id character varying(100),
    verweis_website text,
    clearingdate timestamp without time zone,
    trackingcode text,
    waehrungsfaktor numeric(9,2),
    remote_adresse character varying(20),
    auftragsdetail_id integer,
    neuzugangs_datum timestamp without time zone DEFAULT now() NOT NULL,
    neuzugang_bediener character varying(100) DEFAULT 'ani'::character varying NOT NULL,
    aenderungs_datum timestamp without time zone DEFAULT now() NOT NULL,
    aenderung_bediener character varying(100) DEFAULT 'ani'::character varying NOT NULL
);  
    -- table bestellung_details

    CREATE TABLE ani.bestellung_details (
    lfd_nr numeric NOT NULL,
    bestell_nr numeric NOT NULL,
    artikel_nummer character varying(100),
    artikel_nummer_shopware character varying(100),
    artikel_id_shopware character varying(100),
    anzahl numeric,
    preis numeric(9,2),
    pseudo_preis numeric(9,2),
    steuersatz numeric(5,2),
    steuer_id numeric(3,0),
    status_id character varying(4),
    number character varying(10),
    versendet character varying(3),
    versandgruppe character varying(3),
    mode character varying(4),
    neuzugangs_datum timestamp without time zone DEFAULT now() NOT NULL,
    neuzugang_bediener character varying(100) DEFAULT 'ani'::character varying NOT NULL,
    aenderungs_datum timestamp without time zone DEFAULT now() NOT NULL,
    aenderung_bediener character varying(100) DEFAULT 'ani'::character varying NOT NULL
);
    -- table lov_artikel

    CREATE TABLE ani.lov_artikel (
    artikelnummer text NOT NULL,
    ean_nummer text,
    hersteller text,
    artikelgruppe text,
    bezeichnung text,
    einheit text,
    gesperrt boolean,
    gewicht_in_kg_netto numeric,
    gewicht_in_kg_brutto numeric,
    verpackungseinheit numeric,
    mass_laenge numeric,
    mass_breite numeric,
    mass_hoehe numeric,
    mass_einheit text,
    shelf_life_in_months numeric,
    ean_primary_unit_edi text,
    ean_secondary_unit_edi text,
    type_of_pallet text,
    height_pallet_in_cm numeric,
    secondary_units_per_tertiary_unit text,
    secondary_units_per_pallet_layer text,
    tertiary_units_per_pallet text,
    ean_tertiary_unit_edi text,
    number_of_pallet_layers text,
    secondary_units_per_pallet text,
    width_tertiary_unit_in_cm numeric,
    height_tertiary_unit_in_cm numeric,
    expiring_item boolean,
    length_tertiary_unit_in_cm numeric,
    gross_weight_tertiary_unit_in_kg numeric,
    recipe_number text,
    artikelobergruppe text,
    gross_weight_primary_unit_in_kg numeric,
    net_weight_primary_unit_in_kg numeric,
    artikelklasse text,
    herstellernummer text,
    produkt text,
    produktgruppe text,
    produktgruppe_detail text,
    produktgruppe_mmr text,
    produktgruppe_update text,
    flavour text,
    brand text
);

"""
        try:
            cursor_test.execute(query)
            connection_test.commit()
        except psycopg2.Error as err:
            print(err)
        else:
            print("OK")

        insert_data_query = """

        -- table bestellung

        INSERT INTO bestellung VALUES (17, 341, 1, '931f9a543d9f89a88aecd25ec71a53cc', '2017-04-11 17:46:50', 27.98, 0.00, 'EUR', NULL, NULL, 7, NULL, 11, 9, 29.94, 0.00, NULL, '', '18.05.2017 15:46:01
Reversal ShortID: 3590.4148.4986
Amount: 29.94 EUR
Original Amount: 29.94 EUR

21.04.2017 16:01:41
Finalize ShortID: 3567.0961.5592
Amount: 29.94 EUR
Original Amount: 29.94 EUR

11.04.2017 17:48:27
Reservation ShortID: 3558.5200.7588
Amount: 29.94 EUR
Original Amount: 29.94 EUR

', '31HA07BC8126F072277E28861E16ADEB', '', '2017-04-21 16:01:41', '456205000715,CE390400617DE', 1.00, '80.147.196.114', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 12770, 1, '0957ca60addee54e36235076744ac2b5', '2017-07-18 12:26:56', 7.94, 3.90, 'EUR', NULL, NULL, 4, NULL, 7, 9, 8.89, 3.28, NULL, '', '18.07.2017 12:28:26
Debit ShortID: 3643.0001.4028
Amount: 8.89 EUR
Original Amount: 8.89 EUR

18.07.2017 12:27:05
Short-Id: 3643.0001.4028

', '31HA07BC811B391E643C3D51F81E43FC', '', '2017-07-18 12:28:26', '', 1.00, '80.147.196.114', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 23820, 1, '977653f16f71dee15478159d3c778523', '2018-01-11 14:29:36', 18.65, 0.00, 'EUR', NULL, NULL, 4, NULL, 7, 9, 19.96, 0.00, NULL, '', '11.01.2018 15:42:36
Debit ShortID: 3796.0377.5152
Amount: 19.96 EUR
Original Amount: 19.96 EUR

11.01.2018 14:29:45
Short-Id: 3796.0377.5152

', '31HA07BC816685FC16A6370BBD8280E6', '', '2018-01-11 15:42:36', '', 1.00, '91.64.119.241', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 25490, 1, '0eae9308b391a5e485cd6ad54e8018d0', '2018-01-25 12:29:46', 25.05, 0.00, 'EUR', NULL, NULL, 4, NULL, 11, 9, 29.81, 0.00, NULL, '', '25.01.2018 12:35:10
Reservation ShortID: 3808.0618.3362
Amount: 29.81 EUR
Original Amount: 29.81 EUR

25.01.2018 12:30:00
Short-Id: 3808.0618.3362

', '31HA07BC811DE52CBB8115328B9E6681', '', NULL, '', 1.00, '80.147.196.114', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 29094, 1, '54628a03ee0154fca9375900e4e0b0f6', '2018-02-19 11:16:08', 16.64, 0.00, 'EUR', NULL, NULL, 4, NULL, 17, 9, 19.80, 0.00, NULL, '', '19.02.2018 11:18:29
Debit ShortID: 3829.6174.3768
Amount: 19.80 EUR
Original Amount: 19.80 EUR

19.02.2018 11:16:21
Short-Id: 3829.6174.3768

', '31HA07BC8197B8D37926ABF9B8788EDC', '', '2018-02-19 11:18:29', '', 1.00, '80.147.196.114', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 30461, 1, 'e286208de7ef2f2374678d136539ff79', '2018-02-27 12:45:06', 19.59, 0.00, 'EUR', NULL, NULL, 4, NULL, 17, 9, 20.96, 0.00, NULL, '', '27.02.2018 12:47:26
Debit ShortID: 3836.5828.3284
Amount: 20.96 EUR
Original Amount: 20.96 EUR

27.02.2018 12:45:18
Short-Id: 3836.5828.3284

', '31HA07BC812B2156AAC52CC1D22023FA', '', '2018-02-27 12:47:26', '', 1.00, '91.64.119.241', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 30464, 1, '2a3d8824d2497e3a9311470f67bfbb99', '2018-02-27 12:53:21', 19.59, 0.00, 'EUR', NULL, NULL, 4, NULL, 17, 9, 20.96, 0.00, NULL, '', '27.02.2018 12:55:21
Debit ShortID: 3836.5877.3284
Amount: 20.96 EUR
Original Amount: 20.96 EUR

27.02.2018 12:53:33
Short-Id: 3836.5877.3284

', '31HA07BC812B2156AAC5284C6737E492', 'https://www.paypal.com/webapps/hermes?flow=1-P', '2018-02-27 12:55:21', '', 1.00, '213.168.74.162', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 88775, 1, 'SuX3EZy2R1IhSTqs39eWvzNZsUO0Osbp', '2018-11-28 16:11:36', 18.66, 0.00, 'EUR', NULL, NULL, 4, NULL, 7, 9, 19.96, 0.00, NULL, '', '28.11.2018 16:11:41
Short-Id: 4073.4429.4386

', '31HA07BC8187BD967F14256CDBBD8853', '', '2018-11-28 16:11:41', '', 1.00, '90.187.0.0', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 93516, 1, 't449jVwfChJAALb5PUBnllB97BwWYyKj', '2019-01-30 16:55:27', 20.07, 0.00, 'EUR', NULL, NULL, 4, NULL, 7, 9, 21.48, 0.00, NULL, '', '30.01.2019 18:07:30
Refund ShortID: 4127.9112.0078
Amount: 21.48 EUR
Original Amount: 21.48 EUR

30.01.2019 17:22:16
Debit ShortID: 4127.9012.5574
Amount: 21.48 EUR
Original Amount: 21.48 EUR

30.01.2019 16:55:33
Short-Id: 4127.9012.5574

', '31HA07BC81A84D55EE7F8CF540099695', '', '2019-01-30 17:22:16', '', 1.00, '95.91.0.0', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 107207, 1, 'GBvGJF7yNpmkt20nDvbinjmU2AG1wbw9', '2019-05-07 15:39:07', 23.81, 0.00, 'EUR', NULL, NULL, 4, NULL, 7, 9, 25.93, 0.00, NULL, '', '09.05.2019 16:12:30
Refund ShortID: 4213.3585.7436
Amount: 25.93 EUR
Original Amount: 25.93 EUR

07.05.2019 16:24:46
Debit ShortID: 4211.6274.5590
Amount: 25.93 EUR
Original Amount: 25.93 EUR

07.05.2019 15:39:13
Short-Id: 4211.6274.5590

', '31HA07BC815DE4F609817DE4F654078F', '', '2019-05-07 16:24:46', '', 1.00, '95.90.0.0', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 111681, 1, 'XMAZbmZsJhTDtLMMmcShEWP8n7Q9HB9g', '2019-06-12 14:10:19', 38.52, 0.00, 'EUR', NULL, NULL, 4, NULL, 7, 9, 41.22, 0.00, NULL, '', '12.06.2019 15:25:20
Refund ShortID: 4242.7189.8358
Amount: 41.22 EUR
Original Amount: 41.22 EUR

12.06.2019 14:20:07
Debit ShortID: 4242.6781.8842
Amount: 41.22 EUR
Original Amount: 41.22 EUR

12.06.2019 14:10:26
Short-Id: 4242.6781.8842

', '31HA07BC819A0F7EF2A61A4061CFF331', '', '2019-06-12 14:20:07', '', 1.00, '90.187.0.0', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 113828, 1, 'Z4c0nni4i903zBXLfb9sYtfZYwTKDtbW', '2019-06-26 13:04:06', 17.90, 0.00, 'EUR', NULL, NULL, 4, NULL, 17, 9, 19.07, 0.00, NULL, '', '26.06.2019 13:53:48
Refund ShortID: 4254.7629.0710
Amount: 19.07 EUR
Original Amount: 19.07 EUR

26.06.2019 13:24:41
Debit ShortID: 4254.7345.1674
Amount: 19.07 EUR
Original Amount: 19.07 EUR

26.06.2019 13:05:04
Short-Id: 4254.7345.1674

2019-06-26 11:04:11
 Short-Id: 4254.7345.1674', '31HA07BC81706682FC731CEA4DD9CAAE', '', '2019-06-26 13:24:41', '', 1.00, '', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 129444, 1, '', '2019-10-28 11:17:57', 20.07, 0.00, 'EUR', NULL, NULL, 0, NULL, 5, 9, 21.48, 0.00, NULL, '', '', '', '', NULL, '', 1.00, '90.187.0.0', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 133713, 1, '', '2019-12-17 12:20:11', 20.07, 0.00, 'EUR', NULL, NULL, 4, NULL, 5, 9, 21.48, 0.00, NULL, '', '', '', '', NULL, '', 1.00, '90.187.0.0', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 133717, 1, '', '2019-12-17 12:30:31', 20.07, 0.00, 'EUR', NULL, NULL, 4, NULL, 5, 9, 21.48, 0.00, NULL, '', '', '', '', NULL, '', 1.00, '90.187.0.0', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');
INSERT INTO ani.bestellung VALUES (17, 142089, 1, '6gDRZQRGDXGeXWro4b7Ejve2MgjYYf5H', '2020-03-26 16:59:54', 21.12, 0.00, 'EUR', NULL, NULL, 4, NULL, 17, 9, 22.60, 0.00, NULL, '', '26.03.2020 20:21:21
Refund ShortID: 4491.7679.2604
Amount: 22.60 EUR
Original Amount: 22.60 EUR

26.03.2020 17:01:15
Debit ShortID: 4491.6477.2618
Amount: 22.60 EUR
Original Amount: 22.60 EUR

26.03.2020 16:59:59
Short-Id: 4491.6477.2618

', '31HA07BC81B0C3507ED83E7D1FAF2560', '', '2020-03-26 17:01:15', '', 1.00, '95.90.0.0', NULL, '2021-02-10 16:53:01.650992', 'ani', '2021-02-10 16:53:01.650992', 'ani');

-- table bestellung_details

INSERT INTO ani.bestellung_details VALUES (125, 101, '24809300', '24809300', '40', 3, 9.99, NULL, 7.00, 4, '101', '100002', '0', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (134, 110, '22035001', '22035001', '87', 1, 52.55, NULL, 19.00, 1, '101', '100003', '0', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (143, 117, '24809300', '24809300', '40', 3, 9.99, NULL, 7.00, 4, '101', '100004', '0', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (144, 117, 'saisonstart17', 'saisonstart17', '4', 1, 0.00, NULL, 19.00, 0, '0', '100004', '0', '0', '2', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (149, 121, '22035001', '22035001.4', '87', 1, 8.75, NULL, 20.00, 1, '101', '100005', '0', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (150, 121, '21442443', '21442443.5', '234', 1, 9.95, NULL, 10.00, 4, '101', '100005', '0', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (178, 140, '24809300', '24809300', '40', 3, 9.99, NULL, 7.00, 4, '3', '100006', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (186, 144, '21581042', '21581042.4', '149', 1, 9.15, NULL, 7.00, 4, '3', '100007', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (187, 144, '21508442', '21508442.5', '236', 1, 6.45, NULL, 7.00, 4, '3', '100007', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (197, 153, '12212238', '12212238.4', '124', 1, 7.55, NULL, 7.00, 4, '3', '100008', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (198, 153, 'saisonstart2017', 'saisonstart2017', '4', 1, 0.00, NULL, 19.00, 0, '3', '100008', '1', '0', '2', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (259, 173, '21570400', '21570400', '74', 1, 38.15, NULL, 7.00, 4, '3', '100009', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (266, 178, '12149353', '12149353.5', '0', 1, 7.95, NULL, 10.00, 4, '3', '100010', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (267, 178, 'saisonstart2017', 'saisonstart2017', '4', 1, 0.00, NULL, 19.00, 0, '3', '100010', '1', '0', '2', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (288, 183, '21433542', '21433542.5', '132', 2, 6.45, NULL, 10.00, 4, '3', '100011', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (289, 183, '21401542', '21401542.5', '154', 1, 6.45, NULL, 10.00, 4, '3', '100011', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (290, 183, '21571400', '21571400.4', '74', 2, 6.35, NULL, 10.00, 4, '3', '100011', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (291, 183, '21424142', '21424142.5', '134', 2, 6.45, NULL, 10.00, 4, '3', '100011', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (292, 183, '21498342', '21498342.4', '70', 1, 5.95, NULL, 10.00, 4, '3', '100011', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (299, 186, '24768646', '24768646.1', '19', 2, 4.99, NULL, 7.00, 4, '3', '100012', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (300, 186, '24763146', '24763146.1', '235', 2, 4.99, NULL, 7.00, 4, '3', '100012', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (301, 186, '21442443', '21442443.5', '234', 1, 9.95, NULL, 7.00, 4, '3', '100012', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (302, 186, '22035001', '22035001.4', '87', 1, 8.75, NULL, 19.00, 1, '3', '100012', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (313, 189, '12154411', '12154411', '46', 1, 20.95, NULL, 7.00, 4, '3', '100013', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (314, 189, '12149354', '12149354.5', '0', 1, 6.45, NULL, 7.00, 4, '3', '100013', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (315, 189, '22563100', '22563100', '98', 1, 31.80, NULL, 7.00, 4, '3', '100013', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (316, 189, '21442443', '21442443', '234', 1, 49.75, NULL, 7.00, 4, '3', '100013', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (317, 189, '24460017', '24460017', '60', 1, 19.75, NULL, 19.00, 1, '3', '100013', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (321, 192, '21571400', '21571400.4', '74', 3, 6.35, NULL, 10.00, 4, '3', '100014', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (322, 192, 'saisonstart2017', 'saisonstart2017', '4', 1, 0.00, NULL, 19.00, 0, '3', '100014', '1', '0', '2', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (329, 196, '12150608', '12150608', '63', 1, 39.95, NULL, 7.00, 4, '3', '100015', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (330, 196, '24768900', '24768900.1', '235', 1, 4.99, NULL, 7.00, 4, '3', '100015', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (331, 196, '24765746', '24765746.1', '235', 1, 4.99, NULL, 7.00, 4, '3', '100015', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (335, 198, '24763146', '24763146.1', '235', 1, 4.99, NULL, 7.00, 4, '3', '100016', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (336, 198, '24762800', '24762800.1', '235', 1, 4.99, NULL, 7.00, 4, '3', '100016', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (337, 198, '12163798', '12163798', '47', 1, 9.99, NULL, 7.00, 4, '3', '100016', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (344, 204, '24781500', '24781500', '136', 1, 29.95, NULL, 7.00, 4, '3', '100017', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (345, 204, 'saisonstart2017', 'saisonstart2017', '4', 1, 0.00, NULL, 19.00, 0, '3', '100017', '1', '0', '2', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (372, 211, '12212370', '12212370.4', '126', 1, 7.55, NULL, 10.00, 4, '3', '100018', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (373, 211, '22032801', '22032801.4', '220', 1, 7.95, NULL, 10.00, 4, '3', '100018', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');
INSERT INTO ani.bestellung_details VALUES (374, 211, '12075177', '12075177.4', '129', 1, 5.95, NULL, 10.00, 4, '3', '100018', '1', '0', '0', '2021-02-10 16:55:43.791317', 'ani', '2021-02-10 16:55:43.791317', 'ani');

-- table lov_artikel

INSERT INTO ani.lov_artikel VALUES ('111880', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Banana 8(12x41g) JP', 'Box', true, 0.492, 0.55, 12, 1, 6.5, 12, 'cm', 12, '4582201291662', NULL, NULL, 115, '8', '9', '63', NULL, '7', '504', 29.4, 14.3, false, 34.7, 4.9, NULL, 'PB', 0.043, 0.041, NULL, NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Banana', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111883', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Green Apple (12x41g) AU', 'Box', true, 0.492, 0.55, 12, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 0, 0, NULL, 'Ausdauertraining', NULL, NULL, 'Supplements & PowerGel', NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Green Apple Caffeinated ', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111884', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Green Apple 8(12x41g) JP', 'Box', true, 0.492, 0.55, 12, 1, 6.5, 12, 'cm', 12, '4902201072731', NULL, NULL, 115, '8', '9', '63', NULL, '7', '504', 29.4, 14.3, false, 34.7, 4.9, NULL, 'PB', 0.043, 0.041, NULL, NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Green Apple Caffeinated ', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111885', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Lemon Lime 8(12x41g) JP', 'Box', true, 0.492, 0.55, 12, 1, 6.5, 12, 'cm', 12, '4582201291655', NULL, NULL, 115, '8', '9', '63', NULL, '7', '504', 29.4, 14.3, false, 34.7, 4.9, NULL, 'PB', 0.043, 0.041, NULL, NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Lime Lemon ', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111886', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Tropical 8(12x41g) JP', 'Box', true, 0.492, 0.55, 12, 1, 6.5, 12, 'cm', 12, '4902201072748', NULL, NULL, 115, '8', '9', '63', NULL, '7', '504', 29.4, 14.3, false, 34.7, 4.9, NULL, 'PB', 0.043, 0.041, NULL, NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Tropical Fruit ', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111887', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Ume 8(12x41g) JP', 'Box', true, 0.492, 0.55, 12, 1, 6.5, 12, 'cm', 12, '4902201072755', NULL, NULL, 115, '8', '9', '63', NULL, '7', '504', 29.4, 14.3, false, 34.7, 4.9, NULL, 'PB', 0.043, 0.041, NULL, NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Ume', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111888', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Vanilla (12x41g) AU', 'Box', true, 0.492, 0.55, 12, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 0, 0, NULL, 'Ausdauertraining', NULL, NULL, 'Supplements & PowerGel', NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Vanilla', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111898', '0', 'PB', 'GEL', 'PWRR RIDE Bar Peanut Caramel (18x55g) AU', 'Box', true, 0.99, 1.081, 18, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 0, 0, NULL, 'Ausdauertraining', NULL, NULL, 'Supplements & PowerGel', NULL, 'BAR', 'Energy Bars', 'Ride Bar', 'Bars', 'Protein High Sugar (Ride)', 'Peanut-Caramel', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111943', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Neutral (12x41g) AU', 'Box', true, 0.492, 0.55, 12, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 0, 0, NULL, 'Ausdauertraining', NULL, NULL, 'Supplements & PowerGel', NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Neutral', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('111944', '0', 'PB', 'GEL', 'POWERBAR POWERGEL Str Bna (12x41g) AU', 'Box', true, 0.492, 0.55, 12, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 0, 0, NULL, 'Ausdauertraining', NULL, NULL, 'Supplements & PowerGel', NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Strawberry-Banana', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12075177', '4029679502109', 'PB', 'GEL', 'PowerGel Shots Cola 4(16x60g)', 'Box', false, 0.96, 1.2, 16, 32, 14.7, 9, 'cm', 12, '4029679500211', '4029679502109', '201|EURO (Holz 120 x 80 cm)', 120, '4', '24', '30', '4029680000000', '5', '120', 34, 23.5, false, 45.5, 5.112, NULL, 'PB', 0.068, 0.06, NULL, NULL, 'GEL', 'Powergel', 'Powergel Shots', 'All Other', 'Core - Shot', 'Cola', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12075177E', '0', 'PB', 'GEL', 'PowerGel Shots Cola 60g', 'Bag', false, 0.06, 0.075, 1, 15, 10.5, 1, 'cm', 12, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '120', NULL, NULL, false, 0, 0, NULL, 'PB', NULL, NULL, NULL, NULL, 'GEL', 'Powergel', 'Powergel Shots', 'All Other', 'Core - Shot', 'Cola', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12075192', '4029679502208', 'PB', 'GEL', 'PowerGel Shots Lemon 4(16x60g)', 'Box', true, 0.965, 1.203, 16, 32, 14.5, 9, 'cm', 12, '4029679500228', '4029679502208', '201|CHEP EURO (Holz 120 x 80 cm)', 119, '4', '6', '30', '4029680000000', '5', '120', 33.6, 31.1, false, 20.8, 5.112, NULL, 'Ausdauertraining', 0.068, 0.06, 'Supplements & PowerGel', NULL, 'GEL', 'Powergel', 'Powergel Shots', 'All Other', 'Core - Shot', 'Lemon', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12087064', '0', 'PB', 'BAR', 'POWERBAR PROTEINTRPTHCh PBCrsp 8(15x55g) US', 'Case', true, 6.6, 7.9, 15, 32.2, 29, 26.1, 'cm', 9.9, '97421090079', NULL, '202|INDUSTRIE (Holz 120 x 100 cm)', 130, '8', '12', '60', NULL, '5', '60', 28, 25, false, 31, 0, NULL, 'Ausdauertraining', 0.0588, 0.055, 'Supplements & PowerGel', NULL, 'BAR', 'Protein Bars', 'ProteinPlus Bar', 'Bars', 'US Protein', 'Chocolate Peanutbutter Crisp Triple Threat', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12087076', '0', 'PB', 'BAR', 'POWERBAR PRFM ENGY Choc PB 8(12x 2.29oz) US', 'Case', true, 6.24, 7.1, 96, 32.6, 31.5, 13.2, 'cm', 12, '97421030044', NULL, '202|INDUSTRIE (Holz 120 x 100 cm)', 132, '8', '12', '120', NULL, '10', '120', 31.5, 13.2, false, 32.6, 0, NULL, 'Ausdauertraining', 0.0677, 0.065, 'Supplements & PowerGel', NULL, 'BAR', 'Energy Bars', 'Energize Bar', 'Bars', 'Core - Bar', 'Chocolate Peanutbutter', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12087077', '0', 'PB', 'BAR', 'POWERBAR PRFM ENGY VanCrsp 8(12x 2.29oz) US', 'Case', true, 6.24, 7.1, 12, 32.6, 31.5, 13.2, 'cm', 12, '97421030037', NULL, '202|INDUSTRIE (Holz 120 x 100 cm)', 132, '8', '12', '120', NULL, '10', '120', 31.5, 13.2, false, 32.6, 7.1, NULL, 'Ausdauertraining', 0.0677, 0.065, 'Supplements & PowerGel', NULL, 'BAR', 'Energy Bars', 'Energize Bar', 'Bars', 'Core - Bar', 'Vanilla Crisp', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12087209', '0', 'PB', 'BAR', 'POWERBAR PERFORMANCE Engy Ban 8(12x 2.29oz) US', 'Case', true, 6.24, 7.1, 96, 32.6, 31.5, 13.2, 'cm', 12, '97421000054', NULL, '202|INDUSTRIE (Holz 120 x 100 cm)', 132, '8', '12', '120', NULL, '10', '120', 31.5, 13.2, false, 32.6, 7.1, NULL, 'Ausdauertraining', 0.0677, 0.065, 'Supplements & PowerGel', NULL, 'BAR', 'Energy Bars', 'Energize Bar', 'Bars', 'Core - Bar', 'Banana', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12128932', '4029679350137', 'PB', 'BAR', 'Natural Energy Cacao Crunch 5(24x40g)', 'Box', true, 0.96, 1.12, 24, 32.3, 14.8, 7.6, 'cm', 12, '4029679350038', '4029679350137', '201|EURO (Holz 120 x 80 cm)', 157.2, '5', '6', '54', '4029680000000', '9', '270', 33, 16, false, 39, 5.6, NULL, 'Ausdauertraining', 0.0422, 0.04, 'Riegel', NULL, 'BAR', 'Energy Bars', 'Natural Energy Bar', 'Bars', 'Natural Energy', 'Cacao Crunch', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12128932E', '0', 'PB', 'BAR', 'Natural Energy Cacao Crunch 40g', 'Bar', true, 0.04, 0.042, 1, 14, 3.8, 2.3, 'cm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 0, 0, NULL, 'Ausdauertraining', 0.0422, 0.04, 'Riegel', NULL, 'BAR', 'Energy Bars', 'Natural Energy Bar', 'Bars', 'Natural Energy', 'Cacao Crunch', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12128933', '4029679350120', 'PB', 'BAR', 'Natural Energy Strawberry & Cranberry 5(24x40g)', 'Box', true, 0.96, 1.12, 24, 32.3, 14.8, 7.6, 'cm', 12, '4029679350021', '4029679350120', '201|EURO (Holz 120 x 80 cm)', 157.2, '5', '6', '54', '4029680000000', '9', '270', 33, 16, false, 39, 5.6, NULL, 'Ausdauertraining', 0.0422, 0.04, 'Riegel', NULL, 'BAR', 'Energy Bars', 'Natural Energy Bar', 'Bars', 'Natural Energy', 'Strawberry & Cranberry', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12128933E', '0', 'PB', 'BAR', 'Natural Energy Strawberry & Cranberry 40g', 'Bar', true, 0.04, 0.045, 1, 14, 3.8, 2.3, 'cm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 0, 0, NULL, 'Ausdauertraining', NULL, NULL, 'Riegel', NULL, 'BAR', 'Energy Bars', 'Natural Energy Bar', 'Bars', 'Natural Energy', 'Strawberry & Cranberry', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12157734', '0', 'PB', 'GEL', 'POWERGEL PRFM Energy BryBlst 4(24x41g) US', 'Case', true, 3.936, 4.85, 96, 31.8, 25.4, 13.1, 'cm', 12, '97421327731', NULL, '202|INDUSTRIE (Holz 120 x 100 cm)', 117, '4', '15', '135', NULL, '9', '135', 25, 13, false, 30, 4.85, NULL, 'Ausdauertraining', 0.043, 0.041, 'Supplements & PowerGel', NULL, 'GEL', 'Powergel', 'Powergel', 'All Other', 'Core - Gel', 'Berry Blast', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12128934', '4029679350113', 'PB', 'BAR', 'Natural Energy Sweet''n Salty Seeds & Pretzels 5(24x40g)', 'Box', true, 0.96, 1.12, 24, 32.3, 14.8, 7.6, 'cm', 12, NULL, '4029679350113', '201|EURO (Holz 120 x 80 cm)', 157.2, '5', '6', '54', '4029680000000', '9', '270', 33, 16, false, 39, 5.6, NULL, 'Ausdauertraining', 0.0422, 0.04, 'Riegel', NULL, 'BAR', 'Energy Bars', 'Natural Energy Bar', 'Bars', 'Natural Energy', 'Sweet''n Salty', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12128934E', '0', 'PB', 'BAR', 'Natural Energy Sweet''n Salty Seeds & Pretzels 40g', 'Bar', true, 0.04, 0.045, 1, 14, 3.8, 2.3, 'cm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 0, 0, NULL, 'Ausdauertraining', NULL, NULL, 'Riegel', NULL, 'BAR', 'Energy Bars', 'Natural Energy Bar', 'Bars', 'Natural Energy', 'Sweet''n Salty', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12136449', '0', 'PB', 'Powder', 'POWERBAR RECOVERY Orange (6x 864g) US', 'Case', true, 5.184, 6.45, 6, 40.2, 27, 21.5, 'cm', 24, '97421392487', NULL, '202|INDUSTRIE (Holz 120 x 100 cm)', 111, '1', '10', '50', NULL, '5', '50', 27, 21.5, false, 40.2, 6.45, NULL, 'Ausdauertraining', 0.1025, 0.864, 'Supplements & PowerGel', NULL, 'Powder', 'Recovery Powder', 'Recovery Active Powder', 'Powders', 'Core - Powder (Recovery)', 'Orange', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12149289', '4029679801608', 'PB', 'Powder', 'ISOACTIVE Lemon, Single Serves 4(20x33g)', 'Box', true, 0.66, 0.797, 20, 28.2, 9.2, 14.5, 'cm', 18, NULL, '4029679801608', NULL, 98.5, '4', '8', '40', '4029680000000', '5', '160', 39.6, 29.5, false, 16.7, 3.452, NULL, 'Ausdauertraining', 0.04, 0.033, 'Fertiggetr├дnke, Getr├дnkepulver & Getr├дnkekonzentra', NULL, 'Powder', 'Sportdrinks', 'Sportdrinks Powder', 'Powders', 'Core - Powder (ISO)', 'Lemon', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12149352', '9002100036256', 'PB', 'Powder', 'ISOACTIVE Lemon 990g (6)', 'Bag', true, 0.99, 1.077, 1, 13, 7, 3, 'cm', 18, '9002100036256', '9002100036256', NULL, 92.1, '6', '12', '36', '9002100000000', '3', '216', 24.3, 25.7, false, 29.8, 0, NULL, 'Ausdauertraining', 1.077, 0.99, 'Fertiggetr├дnke, Getr├дnkepulver & Getr├дnkekonzentra', NULL, 'Powder', 'Sportdrinks', 'Sportdrinks Powder', 'Powders', 'Core - Powder (ISO)', 'Lemon', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12149353', '4029679801905', 'PB', 'Powder', 'ISOMAX Blood Orange, Single Serves, 4(20x50g)', 'Box', true, 1, 1.203, 20, 28.2, 9.2, 14.5, 'cm', 18, '4029679800199', '4029679801905', '201|CHEP EURO (Holz 120 x 80 cm)', 98.5, '4', '8', '40', '4029680000000', '5', '160', 39.6, 29.5, true, 16.7, 3.452, NULL, 'PB', 0.06, 0.05, NULL, NULL, 'Powder', 'Sportdrinks', 'Sportdrinks Powder', 'Powders', 'Core - Powder (ISO)', 'Blood orange', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12149354', '4029679801707', 'PB', 'Powder', 'ISOACTIVE Red Fruit Punch, Single Serves, 4(20x33g)', 'Box', true, 0.66, 0.797, 20, 28.2, 9.2, 14.5, 'cm', 18, '4029679800175', '4029679801707', '201|CHEP EURO (Holz 120 x 80 cm)', 98.5, '4', '8', '40', '4029680000000', '5', '160', 39.6, 29.5, true, 16.7, 3.452, NULL, 'PB', 0.04, 0.033, NULL, NULL, 'Powder', 'Sportdrinks', 'Sportdrinks Powder', 'Powders', 'Core - Powder (ISO)', 'Red Fruit', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150452', '4029679602014', 'PB', 'Powder', 'ProteinPlus 80% Banana 700g (6)', 'Jar', false, 0.7, 0.856, 1, 12, 12, 21.3, 'cm', 24, '4029679602014', '4029679602014', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 5.1, NULL, 'PB', 0.856, 0.7, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Banana', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150609', '4029679602045', 'PB', 'Powder', 'ProteinPlus 80% Chocolate 700g (6)', 'Jar', true, 0.7, 0.856, 1, 12, 12, 21.3, 'cm', 24, '4029679602045', '4029679602045', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 5.1, NULL, 'PB', 0.856, 0.7, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Chocolate', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150612', '4029679602038', 'PB', 'Powder', 'ProteinPlus 80% Vanilla 700g (6)', 'Jar', true, 0.7, 0.856, 1, 12, 12, 21.3, 'cm', 24, '4029679602038', '4029679602038', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 5.1, NULL, 'PB', 0.856, 0.7, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Vanilla', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150614', '4029679602021', 'PB', 'Powder', 'ProteinPlus 80% Strawberry 700g (6)', 'Jar', true, 0.7, 0.856, 1, 12, 12, 21.3, 'cm', 24, '4029679602021', '4029679602021', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 5.1, NULL, 'PB', 0.856, 0.7, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Erdbeere', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150615', '4029679602069', 'PB', 'Powder', 'ProteinPlus 80% Coconut 700g (6)', 'Jar', false, 0.7, 0.856, 1, 12, 12, 21.3, 'cm', 24, '4029679602069', '4029679602069', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 5.1, NULL, 'PB', 0.856, 0.7, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Kokos', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150647', '4029679602090', 'PB', 'Powder', 'ProteinPlus 80% Stracciatella 700g (6)', 'Jar', false, 0.7, 0.856, 1, 12, 12, 21.3, 'cm', 24, '4029679602090', '4029679602090', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 5.136, NULL, 'PB', 0.856, 0.7, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Stracciatella', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150648', '4029679602106', 'PB', 'Powder', 'ProteinPlus 80% Lion 700g (6)', 'Jar', true, 0.7, 0.856, 1, 12, 12, 21.3, 'cm', 18, '4029679602106', '4029679602106', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 5.1, NULL, 'PB', 0.856, 0.7, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Lion', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150670', '4029679630017', 'PB', 'Powder', 'ProteinPlus 92% Chocolate 600g (6)', 'Jar', true, 0.6, 0.756, 1, 12, 12, 21.3, 'cm', 24, '4029679630017', '4029679630017', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '54', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 4.54, NULL, 'PB', 0.756, 0.6, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Chocolate', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150671', '4029679630024', 'PB', 'Powder', 'ProteinPlus 92% Strawberry 600g (6)', 'Jar', true, 0.6, 0.756, 1, 12, 12, 21.3, 'cm', 24, '4029679630024', '4029679630024', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '54', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 4.54, NULL, 'PB', 0.756, 0.6, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Erdbeere', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12150672', '4029679630000', 'PB', 'Powder', 'ProteinPlus 92% Vanilla 600g (6)', 'Jar', true, 0.6, 0.756, 1, 12, 12, 21.3, 'cm', 24, '4029679630000', '4029679630000', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '54', '27', '4029680000000', '3', '162', 25.1, 22.7, true, 37.1, 4.54, NULL, 'PB', 0.756, 0.6, NULL, NULL, 'Powder', 'Protein Powder', 'ProteinPlus Powder', 'Powders', 'Protein Low Sugar (Powder)', 'Vanilla', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12154365', '4029679800106', 'PB', 'Powder', 'ISOACTIVE Lemon 1.320g (6)', 'Jar', true, 1.32, 1.476, 1, 12, 12, 21, 'cm', 18, '4029679800106', '4029679800106', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 37.1, 25.1, true, 22.7, 8.857, NULL, 'PB', 1.476, 1.32, NULL, NULL, 'Powder', 'Sportdrinks', 'Sportdrinks Powder', 'Powders', 'Core - Powder (ISO)', 'Lemon', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12154410', '4029679800113', 'PB', 'Powder', 'ISOACTIVE Orange 1.320g (6)', 'Jar', true, 1.32, 1.476, 1, 12, 12, 21, 'cm', 18, '4029679800113', '4029679800113', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 37.1, 25.1, true, 22.7, 8.857, NULL, 'PB', 1.476, 1.32, NULL, NULL, 'Powder', 'Sportdrinks', 'Sportdrinks Powder', 'Powders', 'Core - Powder (ISO)', 'Orange', 'PowerBar');
INSERT INTO ani.lov_artikel VALUES ('12154411', '4029679800120', 'PB', 'Powder', 'ISOACTIVE Red Fruit Punch 1.320g (6)', 'Jar', true, 1.32, 1.476, 1, 12, 12, 21, 'cm', 18, '4029679800120', '4029679800120', '201|CHEP EURO (Holz 120 x 80 cm)', 83.1, '6', '9', '27', '4029680000000', '3', '162', 37.1, 25.1, true, 22.7, 8.857, NULL, 'PB', 1.476, 1.32, NULL, NULL, 'Powder', 'Sportdrinks', 'Sportdrinks Powder', 'Powders', 'Core - Powder (ISO)', 'Red Fruit', 'PowerBar');

"""
        try:
            cursor_test.execute(insert_data_query)
            connection_test.commit()
        except psycopg2.Error as err:
            print("Data insertion to test_table failed \n" + str(err))
        cursor_test.close()
        connection_test.close()
        testconfig = {
            'host': Psql_DB_HOST,
            'user': Psql_DB_USER,
            'password': Psql_DB_PASSWORD,
            'database': Psql_DB_NAME
        }
        cls.mock_db_config = patch.dict(utils_db_test.config, testconfig)

    @classmethod
    def tearDownClass(cls):
        cnx = psycopg2.connect(
            database="ani",
            host=Psql_DB_HOST,
            user=Psql_DB_USER,
            password=Psql_DB_PASSWORD
        )
        cursor = cnx.cursor()

        # drop test database
        try:
            cursor.execute("DROP DATABASE {}".format(Psql_DB_NAME))
            cnx.commit()
            cursor.close()
        except psycopg2.Error as err:
            print("Database {} does not exists. Dropping db failed".format(Psql_DB_NAME))
        cnx.close()
